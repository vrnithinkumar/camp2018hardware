/*
*************************************************************************************************************************************************************************************************************
FIST IOT WORKSHOP 2017
*************************************************************************************************************************************************************************************************************

Name                  : Temperature Sensor DHT11 Server Send
Hardware Platform     : ESP 12E NodeMcu V3
Toolchain             : Arduino IDE V1.8.3, ESP8266 core for Arduino V2.3.0.
Version               : 1.0
Date created          : 08/08/2017
Author                : Mugilan M 
Contact               : mugilan_july@yahoo.co.in
Description           : Reads temperature data from sensor and send to server via tcp socket every 5 seconds.
Reference             : https://github.com/adafruit/DHT-sensor-library

*************************************************************************************************************************************************************************************************************

Copyright 2017 Mugilan M

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************************************************************************************************************
*/

// Pin Number: DHT11 Data -> NodeMCU D1

#include <ESP8266WiFi.h>
#include "DHT.h"	// Install adafruit-DHT sensor library and Adafruit Unified Sensor library.

#define DHTPIN 5  

#define DHTTYPE DHT11 

WiFiClient client;

const char* ssid = "SSID";
const char* password = "PASSWD";

IPAddress server_ip(192,168,1,101);  

int server_port = 12345;

float temperature = 0;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
   Serial.begin(115200);

   dht.begin();

   WiFi.begin(ssid, password);

   Serial.println("");
   Serial.println("");
   Serial.println("Connecting to WiFi");
 
  while (WiFi.status() != WL_CONNECTED) {
     delay(500);
     Serial.println("Trying to Connect WiFi Network");
  }

    
}


void loop() {

    Serial.println("Starting connection...");

    temperature = dht.readTemperature();

    if (isnan(temperature)) {
    Serial.println("Failed to read Temperature from DHT sensor!");
    return;
    }

    if (client.connect(server_ip, server_port)) {
      Serial.println("Connected to Server");
      client.println("Temperature1 Data:");
      client.println(temperature);

      Serial.println("Closing Connection");
      client.stop();
    }

    else
    {
      Serial.println("Connection Failed");
    }	

    delay(5000);
}

