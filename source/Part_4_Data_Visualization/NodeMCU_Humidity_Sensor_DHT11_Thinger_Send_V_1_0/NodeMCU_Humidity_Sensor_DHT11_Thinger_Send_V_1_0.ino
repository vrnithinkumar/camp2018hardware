/*
*************************************************************************************************************************************************************************************************************
FIST IOT WORKSHOP 2017
*************************************************************************************************************************************************************************************************************

Name                  : 
Hardware Platform     : ESP 12E NodeMcu V3
Toolchain             : Arduino IDE V1.8.3, ESP8266 core for Arduino V2.3.0.
Version               : 1.0
Date created          : 08/08/2017
Author                : Mugilan M 
Contact               : mugilan_july@yahoo.co.in
Description           : Humidity read from DHT11 is sent to server for visualisation on Thinger platform

*************************************************************************************************************************************************************************************************************

Copyright 2017 Mugilan M

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************************************************************************************************************
*/
#define THINGER_SERVER "192.168.1.140"

#include <ESP8266WiFi.h>
#include <ThingerESP8266.h>
#include "DHT.h"

// Pin Number: DHT11 Data -> NodeMCU D1

#define DHTPIN 5  

#define DHTTYPE DHT11 

#define USERNAME "fist"
#define DEVICE_ID "FIST_Devices"
#define DEVICE_CREDENTIAL "onjs5KULPnA$"

#define SSID "FIST"
#define SSID_PASSWORD "iot12345"

float humidity = 0;

DHT dht(DHTPIN, DHTTYPE);

ThingerESP8266 thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL);

void setup() {

  thing.add_wifi(SSID, SSID_PASSWORD);

  dht.begin();

  Serial.begin(115200);
    delay(10);

  thing["humidity1"] >> [](pson& out){
       humidity = dht.readHumidity();

    if (isnan(humidity)) {
    Serial.println("Failed to read Humidity from DHT sensor!");
    return;
    }
    else{
	out = humidity;
        }
 };

}

void loop() {
  thing.handle();
}
