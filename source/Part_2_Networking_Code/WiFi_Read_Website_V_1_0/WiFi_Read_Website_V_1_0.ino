/*
*************************************************************************************************************************************************************************************************************
FIST IOT WORKSHOP 2017
*************************************************************************************************************************************************************************************************************

Name                  : WiFi Read Website
Hardware Platform     : ESP 12E NodeMcu V3
Toolchain             : Arduino IDE V1.8.3, ESP8266 core for Arduino V2.3.0.
Version               : 1.0
Date created          : 08/08/2017
Author                : Mugilan M 
Contact               : mugilan_july@yahoo.co.in
Description           : Connects to Internet via WiFi Access Point and connects to specified website and prints website contents over serial port.

*************************************************************************************************************************************************************************************************************

Copyright 2017 Mugilan M

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************************************************************************************************************
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
 
const char* ssid = "SSID";
const char* password = "PASSWD";
 
const char http_site[] = "www.example.com";
const int http_port = 80;
 
WiFiClient client;

bool flag = true;
 
void setup() {
  Serial.begin(115200);
  
 WiFi.begin(ssid, password);

 Serial.println("");
 Serial.println("");
 Serial.println("Connecting to WiFi");
 
  while (WiFi.status() != WL_CONNECTED) {
     delay(500);
     Serial.println("Trying to Connect WiFi Network");
  }

  if ( !client.connect(http_site, http_port) ) {
    Serial.println("Connection to Website failed");
    while(1);
  }
  
  client.println("GET / HTTP/1.1");
  client.print("Host: ");
  client.println(http_site);
  client.println("Accept: */*");
  client.println("Connection: close");
  client.println();

 
}
 
void loop() {

  if(flag)
{
    while ( client.available() ) {
    char c = client.read();
    Serial.print(c);
  }

  if ( !client.connected() ) {
    Serial.println();
    
    client.stop();
    if ( WiFi.status() != WL_DISCONNECTED ) {
      WiFi.disconnect();
    }
    
    Serial.println("Finished Reading Website.");
    flag=false;
  }
}
  
}
