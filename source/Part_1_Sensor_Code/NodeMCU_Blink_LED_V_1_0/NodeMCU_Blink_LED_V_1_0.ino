/*
*************************************************************************************************************************************************************************************************************
FIST IOT WORKSHOP 2017
*************************************************************************************************************************************************************************************************************

Name                  : Blink LED
Hardware Platform     : ESP 12E NodeMcu V3
Toolchain             : Arduino IDE V1.8.3, ESP8266 core for Arduino V2.3.0.
Version               : 1.0
Date created          : 08/08/2017
Author                : Mugilan M 
Contact               : mugilan_july@yahoo.co.in
Description           : Blinks on-board or external LED every 0.5 Seconds.

*************************************************************************************************************************************************************************************************************

Copyright 2017 Mugilan M

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************************************************************************************************************
*/


#define LED LED_BUILTIN
//#define LED 5

void setup() {
  pinMode(LED, OUTPUT);     
}

void loop() {
  digitalWrite(LED, HIGH);   
  delay(500);                      
  digitalWrite(LED, LOW); 
  delay(500);                      
}
