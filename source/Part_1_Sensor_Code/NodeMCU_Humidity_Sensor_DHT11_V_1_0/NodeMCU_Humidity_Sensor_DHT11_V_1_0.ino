/*
*************************************************************************************************************************************************************************************************************
FIST IOT WORKSHOP 2017
*************************************************************************************************************************************************************************************************************

Name                  : Humidity Sensor DHT11
Hardware Platform     : ESP 12E NodeMcu V3
Toolchain             : Arduino IDE V1.8.3, ESP8266 core for Arduino V2.3.0.
Version               : 1.0
Date created          : 08/08/2017
Author                : Mugilan M 
Contact               : mugilan_july@yahoo.co.in
Description           : Reads humidity data from sensor and serially prints every 2 seconds.
Reference             : https://github.com/adafruit/DHT-sensor-library

*************************************************************************************************************************************************************************************************************

Copyright 2017 Mugilan M

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************************************************************************************************************
*/

// Pin Number: DHT11 Data -> NodeMCU D1

#include "DHT.h"	// Install adafruit-DHT sensor library and Adafruit Unified Sensor library.

#define DHTPIN 5  

#define DHTTYPE DHT11   

DHT dht(DHTPIN, DHTTYPE);

float humidity = 0;

void setup() {
  Serial.begin(115200);
  dht.begin();
}

void loop() {
  delay(2000);

  humidity = dht.readHumidity();

  if (isnan(humidity)) {
    Serial.println("Failed to read Humidity data from DHT sensor!");
    return;
  }
  
  Serial.println(humidity);

}


